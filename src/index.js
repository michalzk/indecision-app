import React from "react";
import ReactDOM from "react-dom";
import IndecisionApp from "./components/IndecisionApp";

//import "normalize.css/normalize.css";
import "../src/styles/styles.scss";

const rootElement = document.getElementById("app");
ReactDOM.render(<IndecisionApp />, rootElement);

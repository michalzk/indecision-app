import React from "react";
import Option from "./Option";

const Options = props => {
  return (
    <div>
      <div className="widget-header">
        <h3 className="widget-header__title">Your Options</h3>
        <button
          className="button button--link"
          onClick={props.handleRemoveAll}
          disabled={!props.hasOptions()}
        >
          Remove all!
        </button>
      </div>
      {!props.hasOptions() && (
        <p className="widget__message">Please, add some options first!</p>
      )}
      {props.options.map((option, index) => (
        <Option
          key={index}
          count={index}
          optionText={option}
          handleDeleteOption={props.handleDeleteOption}
        />
      ))}
    </div>
  );
};

export default Options;

import React from "react";

const Option = props => {
  return (
    <div className="option">
      <p className="option__text">
        {props.count + 1}. {props.optionText}
      </p>
      <button
        className="button button--link"
        onClick={() => {
          props.handleDeleteOption(props.optionText);
        }}
      >
        X
      </button>
    </div>
  );
};

export default Option;

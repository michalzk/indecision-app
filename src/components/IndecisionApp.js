import React from "react";
import AddOption from "./AddOption";
import Options from "./Options";
import Header from "./Header";
import Action from "./Action";
import OptionModal from "./OptionModal";

class IndecisionApp extends React.Component {
  state = {
    options: [],
    selectedOption: undefined,
    hasOptions() {
      return this.options.length > 0;
    }
  };
  handleAddOption = option => {
    if (!option) {
      return "Enter valid value to add item";
    } else if (this.state.options.indexOf(option) > -1) {
      return "This option already exists";
    }

    this.setState(prevState => ({ options: [...prevState.options, option] }));
  };
  handlePick = () => {
    if (this.state.hasOptions()) {
      const randomNumber = Math.floor(
        Math.random() * this.state.options.length
      );
      const option = this.state.options[randomNumber];
      this.setState({ selectedOption: option });
    }
  };
  handleCloseModal = () => {
    this.setState({ selectedOption: undefined });
  };
  handleDeleteOption = option => {
    const arr = [...this.state.options];
    const pos = arr.indexOf(option);
    arr.splice(pos, 1);
    this.setState({ options: [...arr] });
  };
  handleRemoveAll = () => {
    this.setState({ options: [] });
  };
  componentDidMount() {
    try {
      const json = localStorage.getItem("options");
      const options = JSON.parse(json);
      if (options) {
        this.setState({ options });
      }
    } catch (e) {
      alert("Error while loading options!");
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.options.length !== this.state.options.length) {
      const json = JSON.stringify(this.state.options);
      localStorage.setItem("options", json);
    }
  }

  render() {
    return (
      <div>
        <Header />
        <div className="container">
          <Action
            options={this.state.options}
            hasOptions={this.state.hasOptions}
            handlePick={this.handlePick}
          />
          <div className="widget">
            <Options
              hasOptions={this.state.hasOptions}
              options={this.state.options}
              handleRemoveAll={this.handleRemoveAll}
              handleDeleteOption={this.handleDeleteOption}
            />
            <AddOption
              options={this.state.options}
              handleAddOption={this.handleAddOption}
            />
          </div>
          <OptionModal
            selectedOption={this.state.selectedOption}
            handleCloseModal={this.handleCloseModal}
          />
        </div>
      </div>
    );
  }
}

IndecisionApp.defaultProps = {
  options: []
};

export default IndecisionApp;
